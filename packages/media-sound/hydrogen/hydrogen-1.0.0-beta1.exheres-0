# Copyright 2009, 2010 Ingmar Vanhassel
# Copyright 2017 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user='hydrogen-music' tag=${PV} ]
require cmake freedesktop-desktop gtk-icon-cache

SUMMARY="Advanced Drum Machine"
HOMEPAGE="http://www.hydrogen-music.org/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    alsa
    jack
    ladspa
    lash
    portmidi [[ description = [ MIDI backend, interface to alsa sound card ] ]]
    pulseaudio
"

DEPENDENCIES="
    build+run:
        app-arch/libarchive
        media-libs/libsndfile[>=1.0.17][flac][vorbis]
        x11-libs/qtbase:5
        x11-libs/qtxmlpatterns:5
        alsa? ( sys-sound/alsa-lib )
        jack? ( media-sound/jack-audio-connection-kit )
        ladspa? ( media-libs/liblrdf )
        lash? ( media-sound/lash )
        portmidi? ( media-libs/portmidi )
        pulseaudio? ( media-sound/pulseaudio )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/hydrogen-0.9.7-Use-CMAKE_INSTALL_FULL_DATAROOTDIR.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DWANT_CPPUNIT:BOOL=FALSE
    -DWANT_DEBUG:BOOL=FALSE
    -DWANT_LIBARCHIVE:BOOL=TRUE
    -DWANT_LRDF:BOOL=FALSE
    -DWANT_OSC:BOOL=FALSE
    -DWANT_OSS:BOOL=FALSE
    -DWANT_PORTAUDIO:BOOL=FALSE
    -DWANT_RUBBERBAND:BOOL=FALSE
    -DWANT_SHARED:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_WANTS=(
    "alsa ALSA"
    "jack JACK"
    "jack JACKSESSION"
    "ladspa LADSPA"
    "ladspa LRDF"
    "lash LASH"
    "portmidi PORTMIDI"
    "pulseaudio PULSEAUDIO"
)

src_install() {
    cmake_src_install

    # Remove empty directory
    edo rmdir "${IMAGE}"/usr/share/${PN}/data/doc

    insinto /usr/share/icons/hicolor/scalable/apps
    doins "${CMAKE_SOURCE}"/data/img/gray/h2-icon.svg
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

