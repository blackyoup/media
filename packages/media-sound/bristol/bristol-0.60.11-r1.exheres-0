# Copyright 2012 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix='tar.gz' ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Emulation package for a number of different 'classic' synthesisers"
DESCRIPTION="
Bristol is an emulation package for a number of different 'classic' synthesisers
including additive and subtractive and a few organs. The application consists of
the engine, which is called bristol, and its own GUI library called brighton
that represents all the emulations.
"

BUGS_TO="alip@exherbo.org"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="alsa"

# fails to link with pulseaudio (last checked: 0.60.11)
DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-proto/xorgproto
    build+run:
        media-sound/jack-audio-connection-kit
        alsa? ( sys-sound/alsa-lib )
"

# --disable-pulseaudio enables pulseaudio (last checked: 0.60.11)
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-liblo
    --disable-oss
    --disable-static
    --disable-version-check
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( alsa )

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( HOWTO )

src_prepare() {
    edo sed -e "s:pkg-config:$(exhost --tool-prefix)&:g" \
            -i {bin,bristol}/Makefile.am
    edo sed -e 's:BRISTOL_DIR=$prefix:BRISTOL_DIR=/usr:g' \
            -i configure.ac

    autotools_src_prepare
}

src_install() {
    default

    edo find "${IMAGE}"/usr/share/bristol -depth -type d -empty -delete
}

