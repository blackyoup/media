# Copyright 2015 Jonathan Dahan <jonathan@jonathan.is>
# Distributed under the terms of the GNU General Public License v2

require github [ user=obsproject ] cmake freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Open source broadcasting software for live streaming and recording"
HOMEPAGE+=" https://obsproject.com"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    alsa
    fdk-aac [[ description = [ Support for AAC encoding using the Frauenhofer AAC Codec Library ] ]]
    jack
    pulseaudio
    rtmps [[ description = [ Support RTMPS secure steaming via mbedTLS ] ]]
    scripting [[ description = [ Support for Lua and Python scripts ] ]]
    speex [[ description = [ Support for speexdsp-based noise suppression (Noise Gate) filter ] ]]
    v4l
    vlc [[ description = [ Support adding video playlists via libvlc ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/jansson[>=2.5]
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/speexdsp
        media-libs/x264
        net-misc/curl
        sys-apps/dbus
        sys-libs/zlib
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libXfixes
        x11-libs/libxcb
        x11-libs/qtbase:5[gui]
        x11-libs/qtsvg:5
        x11-libs/qtx11extras:5
        alsa? ( sys-sound/alsa-lib )
        fdk-aac? ( media-libs/fdk-aac )
        jack? ( media-sound/jack-audio-connection-kit )
        providers:ffmpeg? ( media/ffmpeg[h264][vorbis] )
        providers:libav? ( media/libav[h264][vorbis] )
        pulseaudio? ( media-sound/pulseaudio )
        rtmps? ( dev-libs/mbedtls )
        scripting? (
            dev-lang/LuaJIT
            dev-lang/python:*[>=3.4]
            dev-lang/swig
        )
        speex? ( media-libs/speexdsp )
        v4l? (
            media-libs/v4l-utils
            providers:eudev? ( sys-apps/eudev )
            providers:systemd? ( sys-apps/systemd )
        )
        vlc? ( media/vlc )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_CAPTIONS:BOOL=TRUE
    -DCHECK_FOR_SERVICE_UPDATES:BOOL=FALSE
    -DCMAKE_DISABLE_FIND_PACKAGE_ImageMagick:BOOL=TRUE
    -DDISABLE_DECKLINK:BOOL=TRUE
    -DENABLE_FREETYPE:BOOL=TRUE
    -DENABLE_UI:BOOL=TRUE
    -DINSTALLER_RUN:BOOL=FALSE
    -DLIBOBS_PREFER_IMAGEMAGICK:BOOL=FALSE
    -DOBS_VERSION_OVERRIDE:STRING=${PV}
    -DUNIX_STRUCTURE:BOOL=TRUE
    -DUSE_LIBC++:BOOL=FALSE
    -DUSE_XDG:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    '!scripting DISABLE_LUA'
    '!scripting DISABLE_PYTHON'
    '!speex DISABLE_SPEEXDSP'
)

CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'scripting SCRIPTING'
)

CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'rtmps RTMPS'
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'alsa ALSA'
    'fdk-aac Libfdk'
    'jack Jack'
    'pulseaudio PulseAudio'
    'v4l LibUDev'
    'v4l Libv4l2'
    'vlc LibVLC'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE'
)

obs-studio_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

obs-studio_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

